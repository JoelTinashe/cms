package com.example.demo.Filters;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

@Component
public class MyFilter3 implements Filter {
    @Override
    @Order(1)
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        System.out.println("Filter 3 called");
        filterChain.doFilter(servletRequest,servletResponse);

    }
}
