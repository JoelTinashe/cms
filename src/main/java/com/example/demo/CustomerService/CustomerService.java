package com.example.demo.CustomerService;

import com.example.demo.Customer.Customer;

import com.example.demo.DataBaseOperation.CustomerDbase;
import com.example.demo.Exceptions.CustomerNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
@Service
@Component

public class CustomerService {
    private int customerIdCount=1;

    @Autowired
    private CustomerDbase customerDbase;

    private List<Customer> customerList = new CopyOnWriteArrayList<>();

     public Customer addCustomer(Customer customer){
//         customer.setCistomerId(customerIdCount);
//         customerList.add(customer);
////        customerList.add(customer);
//         customerIdCount++;
//         return customer;
         //adding customer into the databse
         return customerDbase.save(customer);

      }

    public List<Customer> getCustomers() {
      //  return customerList;
        return customerDbase.findAll();
    }

    public Customer getCustomersingle(int customerId){
         //handiling expections

        Optional<Customer> customerOptional = customerDbase.findById(customerId);
        if (!customerOptional.isPresent()){
            throw new CustomerNotFoundException("Customer record Not available");
        }
  // gete a specific customer
//      return  customerList
//                 .stream()
//                 .filter(c->c
//                          .getCustomerId()==customerId)
//                 .findFirst().get();
        return customerOptional.get();
    }

    public Customer updateCustomer(int customerId,Customer customer ){
         customer.setCistomerId(customerId);

//           customerList
//                   .stream()
//                   .forEach(c->{
//                       if (c.getCustomerId()==customerId){
//
//                           c.setCustomerFirstName(customer.getCustomerFirstName());
//                           c.setGetCustomerLastName(customer.getGetCustomerLastName());
//                           c.setEmail(customer.getEmail());
//
//                       }
//                   });
//
//           return customerList
//                   .stream()
//                   .filter(c->c.getCustomerId()==customerId)
//                   .findFirst()
//                   .get();
        return customerDbase.save(customer);
    }
    public void  deleteCustomer(int customerId){
         //delete a specific customer by passing an Id
//         customerList
//                 .stream().forEach(c->{
//                      if (c.getCustomerId()==customerId);
//                      customerList.remove(c);
//         });

        customerDbase.deleteById(customerId);


    }

}
