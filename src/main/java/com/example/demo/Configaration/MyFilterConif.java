package com.example.demo.Configaration;

import com.example.demo.Filters.MynewFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyFilterConif {
    public FilterRegistrationBean<MynewFilter> registrationBean(){

        FilterRegistrationBean<MynewFilter> registrationBean=new FilterRegistrationBean<>();
        registrationBean.setFilter(new MynewFilter());
        registrationBean.addUrlPatterns("/customrs/*");

        return registrationBean;
    }
}
