package com.example.demo.DataBaseOperation;

import com.example.demo.Customer.Customer;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerDbase extends CrudRepository<Customer,Integer> {
    @Override
     List<Customer> findAll();
}
