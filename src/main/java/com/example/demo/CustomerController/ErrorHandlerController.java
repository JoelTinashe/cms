package com.example.demo.CustomerController;

import com.example.demo.Exceptions.ApplictionErrorHandler;
import com.example.demo.Exceptions.CustomerNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

 @RestController
 @ControllerAdvice
public class ErrorHandlerController extends ResponseEntityExceptionHandler {
    @Value("${api_doc_url}")
    private String errorDetail;

          @ExceptionHandler(CustomerNotFoundException.class)
    public ResponseEntity<ApplictionErrorHandler> handleCustomerNotFoundException(CustomerNotFoundException exception,
                                          WebRequest webRequest){


        ApplictionErrorHandler errorHandler = new ApplictionErrorHandler();
        errorHandler.setCodeError(404);
        errorHandler.setErrorMessage(exception.getMessage());
        errorHandler.setErrorDetails(errorDetail);
        return new ResponseEntity<>(errorHandler, HttpStatus.NOT_FOUND);


    }
}
