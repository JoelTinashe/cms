package com.example.demo.CustomerController;

import com.example.demo.Customer.Customer;
import com.example.demo.CustomerService.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

 @RestController
 @RequestMapping(value = "/customers")

public class CustomerController {

    private CustomerService customerService;
    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer){

        return customerService.addCustomer(customer);
    }

    @GetMapping
    public List<Customer> getCustomers(){

        return customerService.getCustomers();
    }

    @GetMapping(value = "/{customerId}")
    public Customer getSingleCustomer(@PathVariable("customerId") int customerId){
     return  customerService.getCustomersingle(customerId);

    }

    @PutMapping(value = "/{customerId}")
    public Customer updateCustomer(@PathVariable("customerId") int customerId, @RequestBody Customer customer){

        return customerService.updateCustomer(customerId,customer);

    }
    @DeleteMapping(value = "{customerId}")
    public void deleteCustomer(@PathVariable("customerId") int customerId){

        customerService.deleteCustomer(customerId);
    }
}
