package com.example.demo.Exceptions;


//since we use the class in the error exceptioncontroller
//@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomerNotFoundException extends RuntimeException{

    public CustomerNotFoundException(String message){
        super(message);

    }
}
